import './assets/css/global.css';
import './App.css';
import Home from './pages/home';
import SingleCatPage from './pages/single-cat';
import {
    BrowserRouter as Router,
    Routes,
    Route
} from 'react-router-dom';

function App() {
    return (
        <Router>
           <Routes>
                 <Route exact path='/' element={<Home/>}></Route>
                 <Route exact path='cat'>
                    <Route path=":id" element={<SingleCatPage/>}></Route>
                 </Route>
          </Routes>
       </Router>
    );
}

export default App;
