import '../assets/css/components/breed-image-card.css';
import React from 'react';

class BreedImageCard extends React.Component {
    render () {
        return <div className="component-breed-image-card">
            { this.props.breed_image
                ? <img key={this.props.breed_image.id}
                                className="breed-image"
                                src={this.props.breed_image.url}
                                alt={this.props.breed_image.breeds[0].name + ' image ' + this.props.breed_image.id}/>
                : null
            }
            { this.props.breed_image
                ? <div className="breed-image-link-container">
                    <a className="button-link breed-image-link" href={ "/cat/" + this.props.breed_image.id}> View Details </a>
                  </div>
                : null
            }
        </div> ;
    }
}
 
export default BreedImageCard;