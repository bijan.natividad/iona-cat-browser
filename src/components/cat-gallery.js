import '../assets/css/components/cat-gallery.css';
import React from 'react';
import CatAPIService from '../services/cat-api';
import BreedImageCard from './breed-image-card';

class CatGallery extends React.Component{
    static defaultProps = {
        breed: null
    }

    constructor (props) {
        super(props)
        this.catAPIService = new CatAPIService();
        this.state = {
            breed_images : null,
            current_page : 1,
            image_limit: 10,
            show_load_more_button: false
        }
    }

    componentDidUpdate (prevProps) {
        if (prevProps.breed === this.props.breed) {
            return;
        }

        this.catAPIService.getBreedImages(
            this.props.breed,
            1,
            this.state.image_limit
        ).then((images) => {
            let show_load_more = (images.length >= this.state.image_limit); 

            this.setState({
                current_page: 1,
                breed_images: images,
                show_load_more_button: show_load_more
            });
        }).catch(error => {
            alert("Apologies but we could not load new cats for you at this time! Miau!");
        });
    }

    loadMoreImages() {
        this.catAPIService.getBreedImages(
            this.props.breed,
            this.state.current_page + 1,
            this.state.image_limit
        ).then((images) => {
            let unique_breed_images = this.getUniqueImages(images);

            let breed_images = this.state.breed_images ? images : [];
            breed_images = breed_images.concat(unique_breed_images);

            let show_load_more = (unique_breed_images.length >= this.state.image_limit);

            this.setState({
                breed_images: breed_images,
                current_page: this.state.current_page + 1,
                show_load_more_button: show_load_more
            })
        }).catch(error => {
            alert("Apologies but we could not load new cats for you at this time! Miau!");
        });
    }

    getUniqueImages(breed_images) {
        let unique_breed_images = [];
        for (let breed_image of breed_images) {
            let has_match = false;
            for (let saved_image of this.state.breed_images) {
                if (breed_image.id === saved_image.id) {
                    has_match = true;
                    break;
                }
            }

            if (!has_match) {
                unique_breed_images.push(breed_image);
            }
        }

        return unique_breed_images;
    }

    render () {
        return <div className="component-cat-gallery">
            <div className="breed-image-cards-container">
                { this.state.breed_images && this.state.breed_images.length
                    ? this.state.breed_images.map((breed_image, index) => {
                        return <BreedImageCard key={index} breed_image={breed_image}/>
                    })
                    : null
                }
            </div>
            { this.state.show_load_more_button
                ? <button className="button load-more-button" onClick={(e) => { this.loadMoreImages(e); }}>Load More</button>
                : null
            }
            
        </div>;
    }
}
 
export default CatGallery;