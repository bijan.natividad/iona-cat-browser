class CatAPIService{
    getBreeds() {
        return fetch("https://api.thecatapi.com/v1/breeds").then(res => res.json())
    }

    getBreedImages(breed_id, page = 1, limit = 10) {
        if (!breed_id) {
            return Promise.resolve();
        }
        
        let search_params = new URLSearchParams({
            breed_id: breed_id,
            page: page,
            limit: limit
        })
        return fetch("https://api.thecatapi.com/v1/images/search?" + search_params).then(res => res.json())
    }

    getBreedImage(image_id) {
        if (!image_id) {
            return Promise.resolve();
        }

        return fetch("https://api.thecatapi.com/v1/images/" + image_id).then(res => res.json())
    }
}
 
export default CatAPIService;