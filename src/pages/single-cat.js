import '../assets/css/pages/single-cat-page.css';
import React from 'react';
import CatAPIService from '../services/cat-api'; 
import { useParams } from "react-router-dom";

class SingleCatPage extends React.Component{
    catAPIService;
    constructor (props) {
        super(props)
        this.catAPIService = new CatAPIService();
        this.state = {
            breed_image_url: null,
            breed_image_id: null,
            breed_details: null
        }
    }

    componentDidMount() {
        let breed_image_id = this.props.params.id;
        this.catAPIService.getBreedImage(breed_image_id)
            .then((breed_image) => {
                console.log(breed_image);
                if (!breed_image) {
                    return;
                }

                this.setState({
                    breed_image_url: breed_image.url,
                    breed_image_id: breed_image.id,
                    breed_details: breed_image.breeds[0]
                });
            })
    }

    render () {
        return <div className="single-cat-page-container">
            {
                this.state.breed_details
                ? <div className="single-cat-card">
                    <div className="back-button-container">
                        <a className="button-link back-button" href={"/?breed=" + this.state.breed_details.id}>
                            Back
                        </a>
                    </div>
                    <img className="breed-image" src={this.state.breed_image_url} alt={this.state.breed_details.name}/>
                    <div className="breed-details-container">
                        <h1 className="breed-name">{this.state.breed_details.name}</h1>
                        <h2 className="breed-origin">Origin: {this.state.breed_details.origin}</h2>
                        <h3 className="breed-temperament">{this.state.breed_details.temperament}</h3>
                        <p className="breed-description">{this.state.breed_details.description}</p>
                    </div>
                </div>
                : null
            }
            
        </div>;
    }
}

let SingleCatPageFunction = (props) => (
    <SingleCatPage
        {...props}
        params={useParams()}
    />
);
 
export default SingleCatPageFunction;