import '../assets/css/pages/homepage.css';
import React from 'react';
import CatAPIService from '../services/cat-api'; 
import CatGallery from '../components/cat-gallery';

class Home extends React.Component {
    catAPIService;
    constructor (props) {
        super(props)
        this.catAPIService = new CatAPIService();
        this.state = {
            breeds: [],
            selected_breed: "",
            show_load_more_button: false
        }
    }

    componentDidMount() {
        let queryParams = new URLSearchParams(window.location.search)
        let breed = queryParams.get("breed");

        this.catAPIService.getBreeds()
            .then((breeds) => {
                this.setState({
                    breeds : breeds
                });

                if (breed) {
                    this.setState({
                        selected_breed: breed
                    })
                }
            })
            .catch(error => {
                alert("Apologies but we could not load new cats for you at this time! Miau!");
            })
    }

    selectBreed(event)
    {
        this.setState({
            selected_breed: event.target.value
        });
    }

    render () {
        return <div className="homepage-container">
            <h1>Cat Browser</h1>
            <label for="breed_select">Breed</label>
            <select id="breed_select" className="breed-select" onChange={(e) => { this.selectBreed(e); }} value={this.state.selected_breed}>
                <option value="">Select breed</option>
                {this.state.breeds.map((breed) => {
                    return <option key={breed.id} value={breed.id}>{breed.name}</option>
                })}
            </select>
            <CatGallery breed={this.state.selected_breed}/>
        </div>;
    }
}
 
export default Home;