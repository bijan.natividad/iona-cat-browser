# IONA Cat Browser

Developer: **Bijan** Natividad

### Instructions
* Install necessary tools (git, node/npm)
* Clone repository to your local directory
* Navigate to cloned repo directory
* Install node_modules
    ``npm install``
* Run application
    ``npm start``