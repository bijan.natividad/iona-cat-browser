module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      less: {
        development: {
            files: {
                'src/assets/css/global.css': 'src/assets/less/global.less',
  
                //Pages
                'src/assets/css/pages/homepage.css': 'src/assets/less/pages/homepage.less',
                'src/assets/css/pages/single-cat-page.css': 'src/assets/less/pages/single-cat-page.less',

                //Components
                'src/assets/css/components/breed-image-card.css': 'src/assets/less/components/breed-image-card.less',
                'src/assets/css/components/cat-gallery.css': 'src/assets/less/components/cat-gallery.less'
            }
        },
      }
    });
  
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-less');
  
    // Default task(s).
    grunt.registerTask('default', ['less']);
  
  };